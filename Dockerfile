# wx_docker:v0.1
# https://hub.docker.com/r/sierramike/wx_docker


# Imagen base Python: https://hub.docker.com/r/arm32v7/python/
FROM arm32v7/python:3.6-stretch

# Instalar modulos en Python
RUN pip3 install --no-cache-dir rpi.gpio \
				adafruit-circuitpython-bme280

# Copiar el script al contenedor
COPY script.py ./

# ejecutar el script
CMD ["python3", "./script.py"]
