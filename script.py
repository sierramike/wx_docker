#!/usr/bin/python3

import RPi.GPIO as GPIO
import time

import board
from adafruit_bme280 import basic as adafruit_bme280


i2c = board.I2C()  # uses board.SCL and board.SDA
bme280 = adafruit_bme280.Adafruit_BME280_I2C(i2c)


GPIO.setmode(GPIO.BCM)
GPIO.setup(20, GPIO.OUT)

while True:
   GPIO.output(20, True)
   time.sleep(1)
   GPIO.output(20, False)
   time.sleep(1)
   print (bme280.temperature)
